![img](file:///C:\Users\刘乐\AppData\Local\Temp\ksohtml26332\wps1.jpg)

**期末项目设计报告**

| 题  目   | 基于Oracle数据库的商品销售系统的设计 |      |              |
| -------- | ------------------------------------ | ---- | ------------ |
| 课   程  | Oracle数据库应用                     |      |              |
| 学  院   | 计算机学院                           |      |              |
| 专  业   | 软件工程                             | 年级 | 2020级       |
| 学生姓名 | 刘乐                                 | 学号 | 201810112318 |
| 指导教师 | 赵卫东                               | 职称 | 教授         |

 

| ***\*评分项\**** | ***\*评分标准\****           | ***\*满分\**** | ***\*得分\**** |
| ---------------- | ---------------------------- | -------------- | -------------- |
| 文档整体         | 文档内容详实、规范，美观大方 | 10             |                |
| 表设计           | 表，表空间设计合理，数据合理 | 20             |                |
| 用户管理         | 权限及用户分配方案设计正确   | 20             |                |
| PL/SQL设计       | 存储过程和函数设计正确       | 30             |                |
| 备份方案         | 备份方案设计正确             | 20             |                |
| **得分合计**     |                              |                |                |

2023 年  5 月  26 日

 

 

 

# **（一）概要设计**

## **一、绪论**

随着现代社会的发展，人们的生活水平逐渐提高，人们更加注重精神粮食的富足，面对时代潮流，电商行业在逐渐壮大，面对海量的数据，电商要花费大量的人力和财力来进行存储和维护，业务具有数据海量化的特点。由于业务数据不断增长带来的压力，决定采用oracle数据库系统来完成商品销售系统的数据库系统设计。

## **二、数据库部署模式**

越来越多的人选择去电商购物、庞大商品数据为电影院带来巨大利润的同时,也带来了信息系统风险的相对集中，这使得电商信息系统连续运行的要求也越来越高。加强信息系统灾备体系建设，保障业务连续运行，已经成为影响影院竞争能力的一个重要因素。对RTO=0/RPO=0的系统决定数据库采用RAC+DataDataGuard模式。根据RAC+DataDataGuard模式的特点，有如下要求: .1 主机与备机在物理.上要分开。为了实现容灾的特性，需要在物理。上分割主机和备机。. .2 进行合理 的设计，充分实现DATAGUARD的功能。 注: RTO ( RecoveryTime object): 恢复时间目标，灾难发生后信息系统从停顿到必须恢复的时间要求。 RPO (Recovery Point Object): 恢复点目标，指一个过去的时间点，当灾难或紧急事件发生时，数据可以恢复到的时间点。

## **三、Oracle数据库**

Oracle Database，又名Oracle RDBMS，或简称Oracle。是甲骨文公司的一款关系数据库管理系统。它是在数据库领域一直处于领先地位的产品。可以说Oracle数据库系统是目前世界上流行的关系数据库管理系统，系统可移植性好、使用方便、功能强，适用于各类大、中、小、微机环境。它是一种高效率、可靠性好的、适应高吞吐量的数据库方案。

## **四、项目概述**

本项目是基于Oracle的商品销售系统的数据库设计，商品销售系统包括两个表空间，主要包括了表用户表,分类表,商品表,客户表等,拥有用户信息录入、订单录入、商品信息录入、商品分类等功能，这些信息部分及订单信息采用了相应的oracle数据库表设计。

 

 

 

# （二）**详细设计**

一、创建表及表空间

创建临时表空间：

```sql
create temporary tablespace demo_temp 

tempfile 'D:\demo\orcl\demo_temp.dbf'

size 100m reuse autoextend on next 20m maxsize unlimited;
```

 

创建表空间：

```sql
create tablespace demo datafile ‘D:\demo\orcl\demo_user.dbf’ 

size 100M reuse autoextend on next 40M maxsize unlimited;
```

 

创建用户并指定表空间

```sql
create user demo identified by demo default tablespace demo temporary tablespace demo_temp;
```

二、数据库创建用户表

```sql
create table TB_USER

(

 id  INTEGER,

 no  VARCHAR2(50),

 pwd  VARCHAR2(50),

 name VARCHAR2(50),

 type CHAR(1)

)
```

![img](file:///C:\Users\刘乐\AppData\Local\Temp\ksohtml26332\wps2.jpg) 

三.创建分类表及查询服装样例

```sql
create table TB_CLASSIFY

(

 id  INTEGER,

 name VARCHAR2(50)

)
```

![img](file:///C:\Users\刘乐\AppData\Local\Temp\ksohtml26332\wps3.jpg) 

 

四. 商品表的创建

```sql
create table TB_COMMODITY

(

 id      INTEGER,

 product_name VARCHAR2(100),

 classify_id  INTEGER,

 model    VARCHAR2(50),

 unit     VARCHAR2(50),

 market_value VARCHAR2(50),

 sales_price  VARCHAR2(50),

 cost_price  VARCHAR2(50),

 img      VARCHAR2(100),

 introduce   VARCHAR2(500),

 num     INTEGER

)
```

 

 

![img](file:///C:\Users\刘乐\AppData\Local\Temp\ksohtml26332\wps4.jpg) 

五. 订单表的创建

```sql
create table TB_ORDER

(

 id      INTEGER,

 commodity_id INTEGER,

 customer_id  INTEGER,

 phone     VARCHAR2(20),

 address    VARCHAR2(100),

 postal_code  VARCHAR2(20),

 order_time  VARCHAR2(50),

 total_amount VARCHAR2(10)

)
```

 

![img](file:///C:\Users\刘乐\AppData\Local\Temp\ksohtml26332\wps5.jpg) 

 

 

六.客户表创建

```sql
create table TB_CUSTOMER

(

 id     INTEGER,

 customer_no VARCHAR2(50),

 name    VARCHAR2(50),

 phone    VARCHAR2(20),

 address   VARCHAR2(100)

)
```

 

![img](file:///C:\Users\刘乐\AppData\Local\Temp\ksohtml26332\wps6.jpg) 

 

 

七.函数

1、录入商品分类

```SQL
create or replace procedure t_addclassify(num int)

as

i int;

begin

i:=0;

while i<num LOOP

insert into TB_CLASSIFY(id,name)values (i,concat('服装',i));

i:=i+1; 

commit; 

end LOOP; 

end;

 

call t_addclassify(100000);
```

2、录入商品

```SQL
create or replace procedure t_addcommodity(num int)

as

i int;

begin

i:=0;

while i<num LOOP

insert into TB_COMMODITY(id,product_name,classify_id,model,unit,market_value,sales_price,cost_price,img,introduce,num)

values (i,concat('UNSPOKEN法式度假夏季挂脖吊带褶皱修身气质无袖连衣裙设计感长裙',i),

i,'XS S M L','元','20','20','20',null,'UNSPOKEN法式度假夏季挂脖吊带褶皱修身气质无袖连衣裙设计感长裙',i);

i:=i+1; 

commit; 

end LOOP; 

end;

 

call t_addcommodity(100000); 
```

3、录入客户信息

```SQL
create or replace procedure t_addcus(num int)

as

i int;

begin

i:=0;

while i<num LOOP

insert into TB_CUSTOMER(id,customer_no,name,phone,address)

values (i,concat('100',i),concat('张三',i),FLOOR(dbms_random.value(100000000,200000000)),concat('xxx地址00',i));

i:=i+1;

commit;

end LOOP;

end;

 

call t_addcus(100000);

 
```

4、录入订单信息

```SQL
create or replace procedure t_addorder(num int)

as

i int;

begin

i:=0;

while i<num LOOP

insert into TB_ORDER(id,commodity_id,customer_id,phone,address,postal_code,order_time,total_amount)

values (i,i,i,FLOOR(dbms_random.value(100000000,200000000)),concat('xxx地址00',i),

FLOOR(dbms_random.value(10000,2000)),to_char(sysdate,'yyyy-MM-dd HH24:mi:ss'),i);

i:=i+1;

commit;

end LOOP;

end;

call t_addorder(100000);
```

 

 

录入用户表数据：

用户权限管理员和普通用户

```sql
INSERT INTO "DEMO"."TB_USER" VALUES ('1', 'admin', 'admin', 'admin', '1');

INSERT INTO "DEMO"."TB_USER" VALUES ('2', '1001', '1001', '1001', '2');
```

 

查询客户编号1000客户的姓名

```sql
CREATE OR REPLACE FUNCTION F_CUS ( c_no IN VARCHAR )

 return VARCHAR2 IS 

 result VARCHAR2 (255);

 begin

  select NAME into result from TB_CUSTOMER where CUSTOMER_NO = c_no;

  return (result);

 end F_CUS;

SELECT F_CUS('1000') from dual

 
```

 

查询张三的订单数量

```sql
create or replace procedure test_count()

as

v_total number(3);

begin

select count(*)into v_total from tb_order o left join TB_CUSTOMER c on o.customer_id=c.id where c.name=cusName;

DBMS_OUTPUT.put_line('总订单：'||v_total);

end;
```

 

call test_count('张三0')

八.数据的备份

```sql
exp demo/demo@orcl file=d:/demo.dmp full=y 
```

 
